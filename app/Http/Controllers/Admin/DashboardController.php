<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LiveAccount;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Helper\MT4Connect;
use Illuminate\Support\Facades\Session;
class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ListController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function main(Request $request)
    {
        $liveAccountDatas = $this->userRepository->getLiveAccountMt4();
        $liveAccountArrays = [];
        foreach($liveAccountDatas as $value){
            $value = $value->toArray();
            $login = $value['login'];
            $getBalanceMT4 = MT4Connect::getAccountInfo($login);
            Session::forget("error");
            if(!$getBalanceMT4){
                // Session::put("error", "Hệ thống bận, quý khách vui lòng thử lại sau");
                return view('dashboard.homepage', compact('liveAccountArrays'));
            }
            $value['balance'] = $getBalanceMT4;
            $liveAccountArrays[] = $value;
        }
        return view('dashboard.homepage', compact('liveAccountArrays'));

    }
}
