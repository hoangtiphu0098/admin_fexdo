@extends('layouts.main')

@section('content')
    <div class="header">
        <div class="header-main">
            <h1>Register sainfx</h1>
            @if ($message = Session::get('error'))
                <div class="alert_infomation">
                    <div class="alert-danger">
                        <strong>{{ $message }}</strong>
                        <span class="close_alert">x</span>
                    </div>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert_infomation">
                    <div class="alert-success">
                        <strong>{{ $message }}</strong>
                        <span class="close_alert">x</span>
                    </div>
                </div>
            @endif
            <div class="header-bottom">
                <div class="header-right w3agile register_form-infomation">
                    <div class="header-left-bottom agileinfo">
                        <form method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <input class="form-control" type="text" placeholder="{{ __('First name') }}" name="first_name"
                                value="{{ $data['first_name'] ?? '' }}" required autofocus>
                            <div class="invalid-feedback">
                                {{ $errors->first('first_name') }}
                            </div>
                            <input class="form-control" type="text" placeholder="{{ __('Last_name') }}"
                                value="{{ $data['last_name'] ?? '' }}" name="last_name" required>
                            <div class="invalid-feedback">
                                {{ $errors->first('last_name') }}
                            </div>
                            <input class="form-control" type="email"
                                value="{{ \Illuminate\Support\Facades\Session::get('email') }}" required readonly
                                name="email">
                            <input class="form-control" type="phonenumber" placeholder="{{ __('Phone number') }}"
                                value="{{ $data['phone_number'] ?? '' }}" name="phone_number" required>
                            <div class="invalid-feedback">
                                {{ $errors->first('phone_number') }}
                            </div>
                            <input class="form-control" type="password" placeholder="{{ __('Password') }}"
                                name="password" required>
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                            <input class="form-control" type="password" placeholder="{{ __('Confirm password') }}"
                                name="password_confirmation" required>
                            <div class="invalid-feedback">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                            <div class="footer_form-register">
                                <div class="register_hear">
                                    <span class="note">Did you have an account ?.</span><a
                                        href="{{ route('login') }}">Login here</a>
                                </div>
                            </div>
                            <input type="submit" value="Register">
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
