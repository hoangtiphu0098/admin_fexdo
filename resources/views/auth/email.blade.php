@extends('layouts.main')

@section('content')
    <div class="header">
        <div class="header-main">
               <h1>Register sainfx</h1>
            <div class="header-bottom">
                <div class="header-right w3agile">
                    <div class="header-left-bottom agileinfo">   
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <input class="form-control" type="email" placeholder="{{ __('E-Mail Address') }}"
                                       name="email" value="{{ old('email') }}" required>
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        <div class="footer_form-register">
                            <div class="register_hear">
                                <span class="note">Did you have an account ?.</span><a href="{{route('login')}}">Login here</a>
                            </div>
                      </div>
                    
                        <input type="submit" value="Send OTP">
                    </form>	
                        
                </div>
                </div>
              
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
