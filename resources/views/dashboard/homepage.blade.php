@extends('layouts.base')

@section('content')
<style>
    th{
        width: 25%;
    }
</style>
    <div class="container-fluid">
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div style="font-size: 16px; font-weight: bold">
            List account open MT4 of {{\Illuminate\Support\Facades\Auth::user()->name}}
        </div>
        <div class="table-responsive" style="margin-top: 70px">
            <table class="table table-striped" data-pagination="true">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Login</th>
                    <th scope="col">Balance</th>
                    <th scope="col">Created_at</th>
                </tr>
                </thead>
                <tbody>
                @foreach($liveAccountArrays as $key => $liveAccount)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <th scope="row">{{ $liveAccount['login'] }}</th>
                        <th scope="row">{{ $liveAccount['balance'] }}</th>
                        <th scope="row">{{ date('Y-m-d', strtotime($liveAccount['created_at'])) }}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/boostrap-datepicker.js') }}"></script>
    <script>
        $('.btn-active').on('click', function () {
            let currentUrl = window.location.origin
            let id = $(this).attr('data-id');
            let status = $(this).attr('data-status');
            if (status == 1) {
                $('.modal-body').html('Bạn có muốn kích hoạt người này không ? ')
            } else {
                $('.modal-body').html('Bạn có muốn hủy kích hoạt người này không ? ')
            }
            let redirectUrl = currentUrl + '/admin/agent/active/' + id + '?status=' + status;
            $("#active-agent").attr('action', redirectUrl);
        })
    </script>
@endsection

