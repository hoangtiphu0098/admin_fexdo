<style>
    .c-sidebar-nav{
        background: white;
        color: black;
    }
    .c-sidebar .c-sidebar-nav-link.c-active{
        color: #3f6ad8;
        font-weight: bold;
        background: #e0f3ff;
    }
    .c-sidebar .c-sidebar-nav-link{
        color: #6c757d;
        /* font-weight: bold; */
    }
</style>
<div class="c-sidebar-brand">
    <a href="https://my.sainfx.com">
        <img class="c-sidebar-brand-full" src="{{ url('images/logo_sainfx.png') }}" width="118" height="46" alt="CoreUI Logo">
    </a>
</div>
<ul class="c-sidebar-nav ps">
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{route('dashboard')}}">
            <i class="fa fa-dashboard"></i>
            Dashboard
        </a>
    </li>
    @if(is_null(\Illuminate\Support\Facades\Auth::user()->admin_id))
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('agent.list') }}">
                <i class="fa fa-user"></i>
                Agent
            </a>
        </li>
    @endif
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{ route('user.list') }}">
            <i class="fa fa-users"></i>
            Customer
        </a>
    </li>
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{ route('account.live') }}">
            <i class="fa fa-home"></i>
            Account
        </a>
    </li>
    @if(\Illuminate\Support\Facades\Auth::user()->role == config('role.admin'))
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('deposit.list') }}">
                <i class="fa fa-credit-card"></i>
                Deposit
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('withdrawal.list') }}">
                <i class="fa fa-credit-card"></i>
                Withdrawal
            </a>
        </li>
    @endif
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{ route('report.trade') }}">
            <i class="fa fa-bar-chart" aria-hidden="true"></i>
            Report
        </a>
    </li>
    @if(\Illuminate\Support\Facades\Auth::user()->role == config('role.staff'))
        @if(is_null(\Illuminate\Support\Facades\Auth::user()->admin_id))
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{ route('agent.link') }}">
                    <i class="fa fa-user"></i>
                    Agent Link
                </a>
            </li>
        @endif
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('customer.link') }}">
                <i class="fa fa-user"></i>
                Customer Link
            </a>
        </li>
    @endif
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
</ul>
</div>
