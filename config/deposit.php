<?php
return [
    'type' => [
        'vifa' => 1,
        'bepay' => 2
    ],
    'type_text' => [
        1 => 'VIFA pay',
        2 => 'e-Banking'
    ],
    'status' => [
        'yes' => 1,
        'no' => 2
    ],
    'status_text' => [
        1 => 'Approved',
        2 => 'Processing'
    ]
];
