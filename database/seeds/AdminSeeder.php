<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Admin;
use SebastianBergmann\ObjectReflector\Exception;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'name' => 'pham hoang',
                'email' => 'phamhoang0098@gmail.com',
                'password' => Hash::make('admin1234'),
                'phone_number' => '0853372031',
                'role' => 1,
                'status' => 1,
            ],
            [
                'name' => 'pham hoang test 1',
                'email' => 'phamhoang0098+1@gmail.com',
                'password' => Hash::make('admin1234'),
                'phone_number' => '0987654321',
                'role' => 2,
                'status' => 2
            ]
            ];
        try{
            foreach($admins as $admin){
                Admin::create($admin);
                DB::commit();
                echo "Them thanh cong";
            }
        }catch(Exception $e){
            DB::rollBack();
            echo "That bai";die;
        }
    }
}
