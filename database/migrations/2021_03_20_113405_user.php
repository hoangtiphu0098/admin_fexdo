<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('phone_number', 50);
            $table->string('application_type', 50);
            $table->string('city', 191)->nullable();
            $table->string('state', 191)->nullable();
            $table->string('zip_code', 191)->nullable();
            $table->string('address', 191)->nullable();
            $table->string('country', 191)->nullable();
            $table->string('copy_of_id', 191)->nullable();
            $table->string('proof_of_address', 191)->nullable();
            $table->string('addtional_file', 191)->nullable();
            $table->string('ib_id', 191)->nullable();
            $table->string('email', 191)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        //
    }
}
