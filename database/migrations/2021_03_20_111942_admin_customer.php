<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdminCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string("name", 191);
            $table->string("email", 191);
            $table->string("email_verified_at", 191)->nullable();
            $table->string("password");
            $table->string("phone_number", 191)->nullable();
            $table->rememberToken();
            $table->tinyInteger("role")->default(2);
            $table->bigInteger('admin_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string("ib_id", 191)->nullable();
            $table->float('commission')->nullable();
            $table->float('staff_commission')->nullable();
            $table->timestamps();
        });

        Schema::create('live_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('login', 191);
            $table->string('group', 191);
            $table->string('leverage', 191);
            $table->string('phone_number', 50)->nullable();
            $table->string('ib_id', 191)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->bigInteger('amount_money');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('type')->default(1);
            $table->string('order_number', 50)->nullable();
            $table->string('sign', 191)->nullable();
            $table->string('bank_name', 191)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('withdrawal_funds', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('login', 20);
            $table->string('currency', 20);
            $table->float('available_balance');
            $table->string('withdrawal_type', 191);
            $table->string('bank_account', 191);
            $table->string('bank_name', 191);
            $table->string('swift_code', 20);
            $table->string('iban');
            $table->string('account_name', 191);
            $table->float('balance');
            $table->string('withdrawal_currency',20);
            $table->float('amount');
            $table->string('account_holder', 191);
            $table->string('bank_branch_name', 191);
            $table->string('bank_address', 191);
            $table->string('note', 191);
            $table->tinyInteger('status')->default(2);
            $table->softDeletes();
            $table->timestamps();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('live_accounts');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('withdrawal_funds');
        //
    }
}
